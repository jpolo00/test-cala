# Test Cala - Julián Enrique Polo
Test for get the Job from Cala-Analytics.com

## Getting started

Para ejecutar el servidor, se debe correr el comando

```
# Crear ambiente virtual
python3 -mvenv .env
source .env/bin/activate

# Instalar requerimientos
pip install -r requirements.txt

# Crear link al archivo de configuración
ln -sf settings/config_local.yaml config.yaml

# Para ejecutar el servidor
python main.py
```
