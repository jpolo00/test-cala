google.charts.load('current', {'packages':['corechart']})
google.charts.setOnLoadCallback(pieChart)
google.charts.setOnLoadCallback(comboChart)


async function get_data(path) {
  observer = await axios.get(path).then((res) => {return res.data})
  this.data = observer.data
  return this.data
}

function pieChart() {
    get_data('http://127.0.0.1:5000/api/v1/analytics/orders/type').then((data) => {
        data = [['Tipo', 'Cantidad']].concat(data)

        data = google.visualization.arrayToDataTable(data);
        chart = new google.visualization.PieChart(document.getElementById('piechart'))
        chart.draw(data, {
            title: 'Órdenes por Tipo',
            width: 900,
            height: 500});
    })
}

function comboChart() {
    get_data('http://127.0.0.1:5000/api/v1/analytics/orders/type/user').then((data) => {


        console.log('Data: ', data)

        data = google.visualization.arrayToDataTable(data);
        chart = new google.visualization.ComboChart(document.getElementById('comboChart'))
        chart.draw(data, {
            title : 'Tipos de Órdenes por Usuarios',
            vAxis: {title: 'Órdenes'},
            hAxis: {title: 'Usuarios'},
            seriesType: 'bars',
            series: {5: {type: 'line'}},
            width: 900,
            height: 500
        });
    })
}

new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data: {
      show: {
          filenames: false
      },
      file_txt: '',
      file_xlsx: '',
    },
    created: async function() {
      this.loadData()
      await this.getFilenames()
      this.showData()
    },
    methods: {
      reloadPage: async function() {
            path = 'http://127.0.0.1:5000/api/v1/update'
            observer = await axios.get(path).then((res) => {
                window.location.reload()
            })
      },
      getFilenames: async function() {
        path = 'http://127.0.0.1:5000/api/v1/filenames'
        observer = await axios.get(path).then((res) => {return res.data})
        this.file_txt = observer.file_txt
        this.file_xlsx = observer.file_xlsx
      },
      loadData: function() {
        this.show.filenames = false
      },
      showData: function() {
        this.show.filenames = true
      }
    }
})