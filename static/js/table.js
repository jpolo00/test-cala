new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data: {
      show: {
          table:false,
          filenames: false,
      },
      file_txt: '',
      file_xlsx: '',
      headers: [
          { text: 'Nombre', align: 'start', sortable: true, value: 'name'},
          { text: 'Número de Orden', sortable: true, value: 'order'},
          { text: 'Tipo de Orden', sortable: true, value: 'order_type'}
        ],
      data_table: []
    },
    created: async function() {
      this.loadTable()
      await this.getFilenames()
      await this.getOrders()
    },
    methods: {
      reloadPage: async function() {
            path = 'http://127.0.0.1:5000/api/v1/update'
            observer = await axios.get(path).then((res) => {
                window.location.reload()
            })
      },
      getFilenames: async function() {
        path = 'http://127.0.0.1:5000/api/v1/filenames'
        observer = await axios.get(path).then((res) => {return res.data})
        this.file_txt = observer.file_txt
        this.file_xlsx = observer.file_xlsx
      },
      getOrders: async function() {
          path = 'http://127.0.0.1:5000/api/v1/orders'
          observer = await axios.get(path).then((res) => {return res.data})
          this.data_table = observer.data
          this.showTable()
      },

      loadTable: function() {
        this.show.table = false
        this.show.filenames = false
      },
      showTable: function() {
        this.show.table = true
        this.show.filenames = true
      }
    }
})