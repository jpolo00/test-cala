from src.domain.order_obj import OrderObj


class XlsxUploadData:
    def __init__(self, xlsx_obj, db_obj, db_model_class, config_obj):
        self.__xlsx_obj = xlsx_obj
        self.__db_obj = db_obj
        self.__db_model_class = db_model_class
        self.__config_obj = config_obj

    def load_orders(self):
        column_map = self.__config_obj.get('column_maps')['orders']
        tmp_data = []

        for row in self.__xlsx_obj.data():
            tmp_dict = {}

            for key in column_map.keys():
                if key in row.keys():
                    tmp_dict[column_map[key]] = row[key]
                else:
                    tmp_dict[column_map[key]] = None

            tmp_data.append(OrderObj(**tmp_dict).data())

        self.__db_obj.add_bulk(objs=list(map(lambda obj: self.__db_model_class(**obj), tmp_data)))
