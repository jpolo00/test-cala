class CsvUploadData:
    def __init__(self, csv_obj, db_obj, db_model_class, config_obj):
        self.__csv_obj = csv_obj
        self.__db_obj = db_obj
        self.__db_model_class = db_model_class
        self.__config_obj = config_obj

    def load_users(self):
        column_map = self.__config_obj.get('column_maps')['users']

        for items in self.__csv_obj.data():
            rows = self.__csv_obj.format_rows(items)
            tmp_data = []

            for row in rows:
                tmp_dict = {}

                for key in column_map.keys():
                    if key in row.keys():
                        tmp_dict[column_map[key]] = row[key]
                    else:
                        tmp_dict[column_map[key]] = None

                tmp_data.append(tmp_dict)
            self.__db_obj.add_bulk(objs=list(map(lambda obj: self.__db_model_class(**obj), tmp_data)))
