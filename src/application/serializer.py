class Serializer:

    @staticmethod
    def orders_count_type_user(data, order_type):
        res = {}

        order_type = list(map(lambda item: item[0].capitalize(), order_type))

        for row in data:
            user_name = row[0].capitalize() + ' ' + row[1].capitalize()

            if user_name not in res.keys():
                res[user_name] = {row[2].capitalize(): row[3]}

            res[user_name].update({row[2].capitalize(): row[3]})

        tmp_table = []

        for user in res.keys():
            tmp_table.append([user] + [0] * len(order_type))

        for index in range(len(tmp_table)):
            row_dict = res[tmp_table[index][0]]
            for key in row_dict.keys():
                index_data = order_type.index(key) + 1
                tmp_table[index][index_data] = row_dict[key]

        tmp_table.insert(0, ['Usuarios'] + order_type)

        return tmp_table

    @staticmethod
    def orders_count_type(data):
        res = []

        for row in data:
            res.append([row[0].capitalize(), row[1]])

        return res

    @staticmethod
    def orders(data):
        res = []

        for row in data:
            tmp_dict = {
                'name': row[0].name.capitalize() + ' ' + row[0].last_name.capitalize(),
                'order': row[1].order,
                'order_type': row[1].type.capitalize(),
            }

            res.append(tmp_dict)

        return res
