def remove_strip(value):
    return value.strip()


def remove_space(value):
    return value.replace(' ', '')


def case_format(value):
    return value.lower()


def replace_value(value, filter_values=[], replace=None):
    if value in filter_values:
        return replace
    return value


def replace_none_value(value, empty=None):
    if value is None:
        return empty
    return value
