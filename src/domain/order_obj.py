from .clean_methods import case_format, remove_space, replace_none_value, replace_value, remove_strip


class OrderObj:
    def __init__(self, user_id, order, type):
        self.user_id = user_id
        self.order = order
        self.type = self.clean(type)

    @staticmethod
    def clean(value):
        res = replace_none_value(value, 'sin_info')
        res = case_format(res)
        res = remove_strip(res)
        res = replace_value(value=res, filter_values=['sin info'], replace='sin_info')
        res = remove_space(res)

        return res

    def data(self):
        return {
            'user_id': self.user_id,
            'order': self.order,
            'type': self.type
        }
