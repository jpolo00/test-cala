from pandas import read_excel, notnull
from os import path as os_path
from sys import path as sys_path
sys_path.append(f'{os_path.dirname(os_path.abspath(__file__))}/../..')

from src.infrastructure.files_folder import FilesFolder as fF


class XlsxLoad:
    def __init__(self, xlsx_path):
        if not fF.check_path(xlsx_path):
            raise Exception(f'XlsxLoad Exception, Excel file no found in {xlsx_path}')
        self.__xlsx_path = xlsx_path

    def data(self):
        tmp_df = read_excel(self.__xlsx_path)
        return tmp_df.where(notnull(tmp_df), None).to_dict(orient='records')
