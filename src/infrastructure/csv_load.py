from pandas import read_csv
from os import path as os_path
from sys import path as sys_path
sys_path.append(f'{os_path.dirname(os_path.abspath(__file__))}/../..')

from src.infrastructure.files_folder import FilesFolder as fF


class CsvLoad:
    def __init__(self, csv_path, delimiter=',', chunk=1000):
        if not fF.check_path(csv_path):
            raise Exception(f'CsvLoad Exception, csv file no found in {csv_path}')

        self.__csv_path = csv_path
        self.__delimiter = delimiter
        self.__chunk = chunk

    def data(self):
        return read_csv(self.__csv_path,
                        delimiter=self.__delimiter,
                        chunksize=self.__chunk,
                        keep_default_na=False,
                        na_values=None)

    @staticmethod
    def format_rows(rows):
        return rows.to_dict(orient='records')
