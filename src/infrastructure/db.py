from os import path as os_path
from sys import path as sys_path
sys_path.append(f'{os_path.dirname(os_path.abspath(__file__))}/../..')

from sqlalchemy import func
from src.infrastructure.models import Users, Orders


class DataBase:
    def __init__(self, db):
        self.__session = db.session

    def add_bulk(self, objs):
        try:
            for obj in objs:
                self.__session.add(obj)
            self.__session.commit()
        except Exception as ex:
            raise Exception(f'Database Exception: add_bulk Exception: {ex}')

    def add(self, obj):
        try:
            self.__session.add(obj)
            self.__session.commit()
        except Exception as ex:
            raise Exception(f'Database Exception: add Exception: {ex}')

    def drop_table(self, obj):
        self.__session.query(obj).delete()

    def get_all_orders(self):
        return self.__session.query(Users, Orders).join(Users).all()

    def get_order_count_type(self):
        return self.__session.query(Orders.type, func.count(Orders.type)).group_by(Orders.type).all()

    def get_order_count_type_user(self):
        return self.__session.query(Users.name, Users.last_name, Orders.type, func.count(Orders.type))\
                            .join(Users) \
                            .filter(Users.dni == Orders.user_id) \
                            .group_by(Users.dni, Orders.type)\
                            .all()
