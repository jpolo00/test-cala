import os


class FilesFolder:
    @staticmethod
    def check_path(path):
        if type(path) is not str:
            raise Exception(f'Files Folder Exception: the path "{path}" is no valid type')

        return os.path.exists(path)
