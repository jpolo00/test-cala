import yaml
from os import path as os_path
from sys import path as sys_path
sys_path.append(f'{os_path.dirname(os_path.abspath(__file__))}/../..')

from src.infrastructure.files_folder import FilesFolder as fF


class Config:
    def __init__(self, config_path=None):
        self.__config_path = 'config.yaml'
        self.__data = {}

        if config_path is not None and type(config_path) is str:
            self.__config_path = config_path

        self.__get_config()

    def __get_config(self):
        if not fF().check_path(self.__config_path):
            raise Exception(f'Config Exception: Config file "{self.__config_path}", No found...')

        with open(self.__config_path) as file:
            tmp_dict = yaml.load(file, Loader=yaml.FullLoader)
            self.__data = dict(map(lambda item: (item.lower(), tmp_dict[item]), tmp_dict.keys()))

    def get(self, key):
        self.__get_config()

        key = key.lower()
        if key not in self.__data.keys():
            raise Exception(f'Config Exception: Value "{key}", no found in configure file {self.__config_path}')

        return self.__data[key]
