from src.application.csv_upload_data import CsvUploadData
from src.application.xlsx_upload_data import XlsxUploadData
from src.application.serializer import Serializer

from src.infrastructure.config import Config
from src.infrastructure.csv_load import CsvLoad
from src.infrastructure.db import DataBase
from src.infrastructure.models import db, Users, Orders
from src.infrastructure.xlsx_load import XlsxLoad

from flask import Flask, jsonify, render_template
from flask_cors import CORS


def create_app(config):
    app = Flask(__name__)
    cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
    app.config['ENV'] = config.get('flask_env')
    app.config['DEBUG'] = config.get('flask_debug')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.get('sqlalchemy_track_modifications')
    app.config['SQLALCHEMY_DATABASE_URI'] = config.get('sqlalchemy_database_uri')

    with app.app_context():
        db.init_app(app)
        db.create_all()

    return app


config_obj = Config()
flask_app = create_app(config=config_obj)
db_obj = DataBase(db=db)
serializer = Serializer()


@flask_app.route('/')
def index():
    db_obj.drop_table(Orders)
    db_obj.drop_table(Users)

    CsvUploadData(csv_obj=CsvLoad(csv_path=config_obj.get('file_txt'), delimiter='\t'),
                  db_obj=db_obj,
                  db_model_class=Users,
                  config_obj=config_obj).load_users()

    XlsxUploadData(xlsx_obj=XlsxLoad(xlsx_path=config_obj.get('file_xlsx')),
                   db_obj=db_obj,
                   db_model_class=Orders,
                   config_obj=config_obj).load_orders()

    return render_template('index.html')


@flask_app.route('/analytics')
def analytics():
    return render_template('analytics.html')


@flask_app.route('/api/v1/filenames', methods=['GET'])
def filenames():
    config_obj.get('file_xlsx')

    return jsonify({
        'file_txt': config_obj.get('file_txt'),
        'file_xlsx': config_obj.get('file_xlsx'),
    })


@flask_app.route('/api/v1/update', methods=['GET'])
def update():
    db_obj.drop_table(Orders)
    db_obj.drop_table(Users)

    CsvUploadData(csv_obj=CsvLoad(csv_path=config_obj.get('file_txt'), delimiter='\t'),
                  db_obj=db_obj,
                  db_model_class=Users,
                  config_obj=config_obj).load_users()

    XlsxUploadData(xlsx_obj=XlsxLoad(xlsx_path=config_obj.get('file_xlsx')),
                   db_obj=db_obj,
                   db_model_class=Orders,
                   config_obj=config_obj).load_orders()

    return jsonify({'message': '200 OK'})


@flask_app.route('/api/v1/analytics/orders/type', methods=['GET'])
def orders_type():
    return jsonify({'data': serializer.orders_count_type(db_obj.get_order_count_type())})


@flask_app.route('/api/v1/analytics/orders/type/user', methods=['GET'])
def orders_type_user():
    return jsonify({'data': serializer.orders_count_type_user(db_obj.get_order_count_type_user(),
                                                              db_obj.get_order_count_type())})


@flask_app.route('/api/v1/orders', methods=['GET'])
def orders():
    return jsonify({'data': serializer.orders(db_obj.get_all_orders())})


if __name__ == '__main__':
    flask_app.run()
